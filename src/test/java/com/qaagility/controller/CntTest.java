package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class CntTest {

@Test
public void checkDiv(){

Cnt count = new Cnt();

assertEquals(4, count.d(12,3));

}

@Test
public void checkDivZero(){

Cnt count = new Cnt();

assertEquals(Integer.MAX_VALUE, count.d(12,0));


}

}
